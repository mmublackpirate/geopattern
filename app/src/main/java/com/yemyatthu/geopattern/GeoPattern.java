package com.yemyatthu.geopattern;

import android.graphics.Color;
import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yemyatthu on 12/12/14.
 */
public class GeoPattern {
  protected static int mBaseWidth;
  protected static int mBaseHeight;
  protected static String mString;
  protected static String mBaseColor;
  protected static String mColor;
  protected static String mHash;
  protected static SVGHelper sMSvgHelper;
  protected static String mPattern;
  protected static String mGenerator;
  protected static String[] mPatternsArray = new String[]{
      "octagons", "overlappingCircles", "plusSigns", "xes", "hexagons",
      "overlappingRings", "plaid", "triangles", "squares", "concentricCircles", "diamonds",
      "nestedSquares",  "trianglesRotated", "chevrons","sineWaves"
  };
  protected static List<String> mPatterns = new ArrayList<String>();

  private static final String FILL_COLOR_DARK = "#222";
  private static final String FILL_COLOR_LIGHT = "#ddd";
  private static final String STROKE_COLOR = "#000";
  private static final String STROKE_OPACITY = "0.02";
  private static final float OPACITY_MIN = (float) 0.02;
  private static final float OPACITY_MAX = (float) 0.15;

  private static final String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})";

  public GeoPattern() {
    this(Long.toString(System.currentTimeMillis()));
  }

  public GeoPattern(String string) {
    this(string, "#933c3c");
  }

  public GeoPattern(String string, int baseWidth, int baseHeight) {
    this(string, "#933c3c", null, null, baseWidth, baseHeight);
  }

  public GeoPattern(String string, String generator, int baseWidth, int baseHeight) {
    this(string, "#933c3c", null, generator, baseWidth, baseHeight);
  }

  public GeoPattern(String string, String baseColor) {
    this(string, baseColor, null);
  }

  public GeoPattern(String string, String baseColor, String color) {
    this(string, baseColor, color, null, 100);
  }

  public GeoPattern(String string,String baseColor, String color, int baseWidth, int baseHeight){
    this(string,baseColor,color,null,baseWidth,baseHeight);
  }

  public GeoPattern(String string, String baseColor, String color, String generator,
      int baseWidth) {
    this(string, baseColor, color, generator, baseWidth, 100);
  }

  public GeoPattern(String string, String baseColor, String color, String generator, int baseWidth,
      int baseHeight) {
    mPatterns.addAll(Arrays.asList(mPatternsArray));
    mBaseWidth = baseWidth;
    mBaseHeight = baseHeight;
    setToString(string);
    setBaseColor(baseColor);
    if (color != null) {
      setColor(color);
    }
    if (generator != null) {
      setGenerator(generator);
    }
    sMSvgHelper = new SVGHelper();
    generateBackground();
    generatePattern();
  }

  public void setToString(String string) {
    try {
      mString = string;
      mHash = SHA1(string);
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  public String getSVG() {
    return sMSvgHelper.toString();
  }

  public void setBaseColor(String baseColor) {
    Pattern pattern = Pattern.compile(HEX_PATTERN);
    Matcher matcher = pattern.matcher(baseColor);
    if (matcher.matches()) {
      mBaseColor = baseColor;
    } else {
      throw new InvalidParameterException(baseColor + " is not a valid hex color");
    }
  }

  public void setColor(String color) {
    Pattern pattern = Pattern.compile(HEX_PATTERN);
    Matcher matcher = pattern.matcher(color);
    if (matcher.matches()) {
      mColor = color;
    } else {
      throw new InvalidParameterException(color + " is not a valid hex color");
    }
  }

  protected void setGenerator(String generator) {
    if (generator == null || mPatterns.contains(generator)) {
      mGenerator = generator;
    } else {
      throw new InvalidParameterException(generator + " is not a valid generator type");
    }
  }

  //generators

  protected static void generateBackground() {
    float hueOffset = map(hexVal(14, 17), 0, 4095, 0, 359);
    float satOffset = hexVal(17, 18);
    float[] baseColor = hexToHSV(mBaseColor);
    String color = mColor;
    baseColor[0] = baseColor[0] - hueOffset;
    if (satOffset % 2 == 0) {
      baseColor[1] = baseColor[1] + satOffset / 100;
    } else {
      baseColor[1] = baseColor[1] - satOffset / 100;
    }
    float rgb[];
    if (color != null) {
      rgb = hexToRgb(color);
    } else {
      rgb = hsvToRgb(baseColor);
    }
    ArrayList<Map<String, String>> args = new ArrayList<>();
    Map<String, String> arg1 = new HashMap<>();
    arg1.put("fill", "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")");
    args.add(arg1);

    sMSvgHelper.rect(0, 0, "100%", "100%", null, args);
  }

  protected void generatePattern() {
    String pattern;
    if (mGenerator == null) {
      if (hexVal(20, 21) < mPatterns.size()) {
        pattern = mPatterns.get(hexVal(20, 21));
      } else {
        pattern = mPatterns.get(hexVal(20, 21) - mPatterns.size());
      }
    } else {
      if (mPatterns.contains(mGenerator)) {
        pattern = mGenerator;
      } else {
        throw new InvalidParameterException("Invalid Genarator");
      }
    }
    switch (pattern) {
      case "octagons":
        octagons();
        break;
      case "overlappingCircles":
        overlappingCircles();
        break;
      case "plusSigns":
        plusSigns();
        break;
      case "hexagons":
        hexagons();
        break;
      case "xes":
        xes();
        break;
      case "overlappingRings":
        overlappingRings();
        break;
      case "plaid":
        plaid();
        break;
      case "triangles":
        triangles();
        break;
      case "squares":
        squares();
        break;
      case "concentricCircles":
        concentricCircles();
        break;
      case "diamonds":
        diamonds();
        break;
      case "nestedSquares":
        nestedSquares();
        break;
      case "trianglesRotated":
        trianglesRotated();
        break;
      case "chevrons":
        chevrons();
        break;
      case "sineWaves":
        sineWaves();
        break;
      default:
        break;
    }
  }

  // Pattern Makers
  public static void hexagons() {
    int scale = hexVal(0, 1);
    float sideLength = map(scale, 0, 15, 8, 60);
    float hexHeight = (float) (sideLength * Math.sqrt(3));
    float hexWidth = sideLength * 2;
    String hex = buildHexagonShape(sideLength);

    sMSvgHelper.setWidth(Math.round((hexWidth * 3) + (sideLength * 3)));
    sMSvgHelper.setHeight(Math.round(hexHeight * 6));

    int i = 0;

    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float dy = (x % 2 == 0) ? (y * hexHeight) : (y * hexHeight + hexHeight / 2);
        float opacity = opacity(val);
        float onePointFiveXSideLengthMinusHalfHexWidth;
        float onePointFiveSideLengthSixMinusHalfHexWidth;
        float dyMinusHalfHexHeight;
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("stroke", STROKE_COLOR);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("stroke-opacity", STROKE_OPACITY);
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("fill-opacity", String.valueOf(opacity));
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("fill", fill);
        styles.add(arg1);
        styles.add(arg2);
        styles.add(arg3);
        styles.add(arg4);
        ArrayList<Map<String, String>> args = new ArrayList<>();
        onePointFiveXSideLengthMinusHalfHexWidth = (float) (x * sideLength * 1.5 - hexWidth / 2);
        dyMinusHalfHexHeight = dy - hexHeight / 2;
        Map<String, String> transform = new HashMap<>();
        transform.put("transform", "translate("
            + onePointFiveXSideLengthMinusHalfHexWidth
            + ","
            + dyMinusHalfHexHeight
            + ")");
        args.add(transform);

        sMSvgHelper.polyline(hex, styles, args);

        // Add an extra one at top-right, for tiling.
        if (x == 0) {
          onePointFiveSideLengthSixMinusHalfHexWidth =
              (float) (6 * sideLength * 1.5 - hexWidth / 2);
          transform.clear();
          transform.put("transform", "translate("
              + onePointFiveSideLengthSixMinusHalfHexWidth
              + ","
              + dyMinusHalfHexHeight
              + ")");
          args.clear();
          args.add(transform);
          sMSvgHelper.polyline(hex, styles, args);
        }

        // Add an extra row at the end that matches the first row, for tiling.
        if (y == 0) {
          float dy2 = (x % 2 == 0) ? (6 * hexHeight) : (6 * hexHeight + hexHeight / 2);
          float dy2MinusHalfHexHeight = dy2 - hexHeight / 2;
          transform.clear();
          transform.put("transform", "translate("
              + onePointFiveXSideLengthMinusHalfHexWidth
              + ","
              + dy2MinusHalfHexHeight
              + ")");
          args.clear();
          args.add(transform);
          sMSvgHelper.polyline(hex, styles, args);
        }

        // Add an extra one at bottom-right, for tiling.
        if (x == 0 && y == 0) {
          onePointFiveSideLengthSixMinusHalfHexWidth =
              (float) (6 * sideLength * 1.5 - hexWidth / 2);
          float fiveHexHeightPlusHalfHexHeight = 5 * hexHeight + hexHeight / 2;
          transform.clear();
          transform.put("transform", "translate("
              + onePointFiveSideLengthSixMinusHalfHexWidth
              + ","
              + fiveHexHeightPlusHalfHexHeight
              + ")");
          args.clear();
          args.add(transform);
          sMSvgHelper.polyline(hex, styles, args);
        }
        i++;
      }
    }
  }

  public static void sineWaves() {
    float period = (float) Math.ceil(map(hexVal(0, 1), 0, 15, 100, mBaseWidth));
    float quarterPeriod = period / 4;
    float xOffset = (float) (period / 4 * 0.7);
    float amplitude = (float) Math.floor(map(hexVal(1, 2), 0, 20, 60, 200));
    float waveWidth = (float) Math.floor(map(hexVal(2   , 3), 0, 20, 60, 200));

    String amplitudeString = Float.toString(amplitude);
    String halfPeriod = Float.toString(period /  2);
    String halfPeriodMinusXOffset = Float.toString(period / 2 - xOffset);
    String periodMinusXOffset = Float.toString(period - xOffset);
    String twoAmplitude = Float.toString(2 * amplitude);
    String onePointFivePeriodMinusXOffset = Float.toString((float) (period * 1.5 - xOffset));
    String onePointFivePeriod = Float.toString((float) (period * 1.5));
    String str = "M0 "+amplitudeString+" C "+xOffset+" 0, "+halfPeriodMinusXOffset+" 0, "+halfPeriod+" "+amplitudeString+" S "+periodMinusXOffset+" "+twoAmplitude+", "+period+" "+amplitudeString+" S "+onePointFivePeriodMinusXOffset+" 0, "+onePointFivePeriod+", "+amplitudeString;
    sMSvgHelper.setWidth(mBaseWidth);
    sMSvgHelper.setHeight(mBaseHeight);

    for (int i = 0; i <= 35; i++) {
      float val = hexVal(i, i+1);
      float opacity = opacity(val);
      String fill =  fillColor(Math.round(val));
      ArrayList<Map<String, String>> styles = new ArrayList<>();
      Map<String, String> arg1 = new HashMap<>();
      arg1.put("opacity", String.valueOf(opacity));
      Map<String, String> arg2 = new HashMap<>();
      arg2.put("stroke-width", String.valueOf(waveWidth)+ "px");
      styles.add(arg1);
      styles.add(arg2);
      ArrayList<Map<String,String>> args = new ArrayList<>();
      Map<String,String> arg3 = new HashMap<>();
      arg3.put("fill","none");
      Map<String,String> arg4 = new HashMap<>();
      arg4.put("stroke",fill);
      float iWaveWidthMinusOnePointFiveAmplitude = (float) (waveWidth * i - amplitude * 1.5);
      float iWaveWidthMinusOnePointFiveAmplitudePlusThirtySixWaveWidth =
          (float) (waveWidth * i - amplitude * 1.5 + waveWidth * 36);
      Map<String, String> transform1 = new HashMap<>();
      transform1.put("transform", "translate("
          + "-"+quarterPeriod
          + ","
          + iWaveWidthMinusOnePointFiveAmplitude
          + ")");
      Map<String, String> transform2 = new HashMap<>();
      transform2.put("transform", "translate("
          + "-"+quarterPeriod
          + ","
          + iWaveWidthMinusOnePointFiveAmplitudePlusThirtySixWaveWidth
          + ")");

      args.add(arg3);
      args.add(arg4);
      args.add(transform1);


      ArrayList<Map<String,String>> args2 = new ArrayList<>();
      args2.add(arg3);
      args2.add(arg4);
      args2.add(transform2);
      sMSvgHelper.path(str,styles,args);
      sMSvgHelper.path(str, styles, args2);    }
  }

  public static void overlappingCircles() {
    int scale = hexVal(0, 1);
    float diameter = map(scale, 0, 15, 25, 200);
    float radius = diameter / 2;
    sMSvgHelper.setWidth(Math.round(radius * 6));
    sMSvgHelper.setHeight(Math.round(radius * 6));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("opacity", String.valueOf(opacity));
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("fill", fill);
        styles.add(arg1);
        ArrayList<Map<String, String>> args = new ArrayList<>();
        args.add(arg2);

        sMSvgHelper.circle(Math.round(x * radius), Math.round(y * radius), Math.round(radius), styles,
            args);
        // Add an extra one at top-right, for tiling.
        if (x == 0) {
          sMSvgHelper.circle(Math.round(6 * radius), Math.round(y * radius), Math.round(radius), styles,
              args);
        }
        // Add an extra row at the end that matches the first row, for tiling.
        if (y == 0) {
          sMSvgHelper.circle(Math.round(x * radius), Math.round(6 * radius), Math.round(radius), styles,
              args);
        }
        // Add an extra one at bottom-right, for tiling.
        if (x == 0 && y == 0) {
          sMSvgHelper.circle(Math.round(6 * radius), Math.round(y * radius), Math.round(radius), styles,
              args);
        }
        i++;
      }
    }
  }

  public void octagons() {
    float squareSize = map(hexVal(0, 1), 0, 15, 10, 60);
    String tile = buildOctogonShape(squareSize);
    sMSvgHelper.setWidth(Math.round(squareSize * 6));
    sMSvgHelper.setHeight(Math.round(squareSize * 6));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        float xSquareSize = x * squareSize;
        float ySquareSize = y * squareSize;
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("fill", fill);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("fill-opacity", String.valueOf(opacity));
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("stroke", STROKE_COLOR);
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("stroke-opacity", STROKE_OPACITY);
        Map<String, String> arg5 = new HashMap<>();
        arg5.put("transform", "translate(" + xSquareSize + ", " + ySquareSize + ")");
        args.add(arg1);
        args.add(arg2);
        args.add(arg3);
        args.add(arg4);
        args.add(arg5);
        sMSvgHelper.polyline(tile, null, args);
        i++;
      }
    }
  }

  public void chevrons() {
    float chevronWidth = map(hexVal(0, 1), 0, 15, 30, 80);
    float chevronHeight = map(hexVal(0, 1), 0, 15, 30, 80);
    String[] chevronList = buildChevronShape(chevronWidth, chevronHeight);
    ArrayList<String> chevron = new ArrayList<>();
    chevron.addAll(Arrays.asList(chevronList));
    sMSvgHelper.setWidth(Math.round(chevronWidth * 6));
    sMSvgHelper.setHeight(Math.round((float) (chevronHeight * 6 * 0.66)));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("stroke", STROKE_COLOR);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("stroke-opacity", STROKE_OPACITY);
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("stroke-width", String.valueOf(1));
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("fill-opacity", String.valueOf(opacity));
        Map<String, String> arg5 = new HashMap<>();
        arg5.put("fill", fill);
        float xChevronWidth = x * chevronWidth;
        float yPointSixSixChevronHeightMinusHalfChevronHeight =
            (float) (y * chevronHeight * 0.66 - chevronHeight / 2);
        float sixPointSixSixChevronHeightMinusHalfChevronHeight =
            (float) (6 * chevronHeight * 0.66 - chevronHeight / 2);
        Map<String, String> transform1 = new HashMap<>();
        transform1.put("transform", "translate("
            + xChevronWidth
            + ","
            + yPointSixSixChevronHeightMinusHalfChevronHeight
            + ")");
        Map<String, String> transform2 = new HashMap<>();
        transform2.put("transform", "translate("
            + xChevronWidth
            + ","
            + sixPointSixSixChevronHeightMinusHalfChevronHeight
            + ")");
        args.add(arg1);
        args.add(arg2);
        args.add(arg3);
        args.add(arg4);
        args.add(arg5);
        args.add(transform1);

        sMSvgHelper.group(chevron, null, args);
        // Add an extra row at the end that matches the first row, for tiling.
        if (y == 0) {
          args.remove(transform1);
          args.add(transform2);
          sMSvgHelper.group(chevron, null, args);
        }
        i++;
      }
    }
  }

  public static void plusSigns() {
    float squareSize = map(hexVal(0, 1), 0, 15, 10, 25);
    float plusSize = squareSize * 3;
    ArrayList<String> plusShape = new ArrayList<>();
    plusShape.addAll(Arrays.asList(buildPlusShape(squareSize)));
    sMSvgHelper.setWidth(Math.round(squareSize * 12));
    sMSvgHelper.setHeight(Math.round(squareSize * 12));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        float dx = (y % 2 == 0) ? 0 : 1;
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("stroke", STROKE_COLOR);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("stroke-opacity", STROKE_OPACITY);
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("fill-opacity", String.valueOf(opacity));
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("fill", fill);

        float t1 = x * plusSize - x * squareSize + dx * squareSize - squareSize;
        float t2 = y * plusSize - y * squareSize - plusSize / 2;

        Map<String, String> arg5 = new HashMap<>();
        arg5.put("transform", "translate(" + t1 + "," + t2 + ")");
        styles.add(arg3);
        args.add(arg1);
        args.add(arg2);
        args.add(arg4);
        args.add(arg5);

        sMSvgHelper.group(plusShape, styles, args);
        // Add an extra column on the right for tiling.
        if (x == 0) {
          float xT1 = 4 * plusSize - x * squareSize + dx * squareSize - squareSize;
          float xT2 = y * plusSize - y * squareSize - plusSize / 2;
          args.remove(arg5);
          arg5.clear();
          arg5.put("transform", "translate(" + xT1 + "," + xT2 + ")");
          args.add(arg5);
          sMSvgHelper.group(plusShape, styles, args);
        }
        // Add an extra row on the bottom that matches the first row, for tiling.
        if (y == 0) {
          float yT1 = x * plusSize - x * squareSize + dx * squareSize - squareSize;
          float yT2 = 4 * plusSize - y * squareSize - plusSize / 2;
          args.remove(arg5);
          arg5.clear();
          arg5.put("transform", "translate(" + yT1 + "," + yT2 + ")");
          args.add(arg5);
          sMSvgHelper.group(plusShape, styles, args);
        }
        // Add an extra one at top-right and bottom-right, for tiling.
        if (x == 0 && y == 0) {
          float xyT1 = 4 * plusSize - x * squareSize + dx * squareSize - squareSize;
          float xyT2 = 4 * plusSize - y * squareSize - plusSize / 2;
          args.remove(arg5);
          arg5.clear();
          arg5.put("transform", "translate(" + xyT1 + "," + xyT2 + ")");
          args.add(arg5);
          sMSvgHelper.group(plusShape, styles, args);
        }
        i++;
      }
    }
  }

  public static void concentricCircles() {
    float scale = hexVal(0, 1);
    float ringSize = map(scale, 0, 15, 10, 60);
    float strokeWidth = ringSize / 5;
    sMSvgHelper.setWidth(Math.round((ringSize + strokeWidth) * 6));
    sMSvgHelper.setHeight(Math.round((ringSize + strokeWidth) * 6));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        float cx = x * ringSize + x * strokeWidth + (ringSize + strokeWidth) / 2;
        float cy = y * ringSize + y * strokeWidth + (ringSize + strokeWidth) / 2;
        float halfRingSize = ringSize / 2;
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("stroke", fill);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("stroke-width", String.valueOf(strokeWidth) + "px");
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("opacity", String.valueOf(opacity));
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("fill", "none");
        args.add(arg4);
        args.add(arg1);
        styles.add(arg2);
        styles.add(arg3);
        sMSvgHelper.circle(Math.round(cx), Math.round(cy), Math.round(halfRingSize), styles, args);
        val = hexVal(39 - i, 40 - i);
        opacity = opacity(val);
        fill = fillColor(Math.round(val));
        float quarterRingSize = ringSize / 4;
        ArrayList<Map<String, String>> newArgs = new ArrayList<>();
        arg4.clear();
        arg4.put("fill", fill);
        Map<String, String> arg6 = new HashMap<>();
        arg6.put("fill-opacity", String.valueOf(opacity));
        newArgs.add(arg4);
        newArgs.add(arg6);
        sMSvgHelper.circle(Math.round(cx), Math.round(cy), Math.round(quarterRingSize), null, newArgs);
        i++;
      }
    }
  }

  public static void overlappingRings() {
    float scale = hexVal(0, 1);
    float ringSize = map(scale, 0, 15, 10, 60);
    float strokeWidth = ringSize / 4;
    sMSvgHelper.setWidth(Math.round(ringSize * 6));
    sMSvgHelper.setHeight(Math.round(ringSize * 6));

    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("stroke", fill);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("stroke-width", String.valueOf(strokeWidth) + "px");
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("opacity", String.valueOf(opacity));
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("fill", "none");
        args.add(arg4);
        args.add(arg1);
        styles.add(arg2);
        styles.add(arg3);

        float ringSizeMinusHalfStrokeWidth = ringSize - strokeWidth / 2;
        sMSvgHelper.circle(Math.round(x * ringSize), Math.round(y * ringSize),
            Math.round(ringSizeMinusHalfStrokeWidth), styles, args);
        // Add an extra one at top-right, for tiling.
        if (x == 0) {
          sMSvgHelper.circle(Math.round(6 * ringSize), Math.round(y * ringSize),
              Math.round(ringSizeMinusHalfStrokeWidth), styles, args);
        }
        // Add an extra row at the end that matches the first row, for tiling.
        if (y == 0) {
          sMSvgHelper.circle(Math.round(x * ringSize), Math.round(6 * ringSize),
              Math.round(ringSizeMinusHalfStrokeWidth), styles, args);
        }
        // Add an extra one at bottom-right, for tiling.
        if (x == 0 && y == 0) {
          sMSvgHelper.circle(Math.round(6 * ringSize), Math.round(6 * ringSize),
              Math.round(ringSizeMinusHalfStrokeWidth), styles, args);
        }
        i++;
      }
    }
  }

  public static void nestedSquares() {
    float blockSize = map(hexVal(0, 1), 0, 15, 4, 12);
    float squareSize = blockSize * 7;
    float dimension = (squareSize + blockSize) * 6 + blockSize * 6;
    sMSvgHelper.setWidth(Math.round(dimension));
    sMSvgHelper.setHeight(Math.round(dimension));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("stroke", fill);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("stroke-width", String.valueOf(blockSize) + "px");
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("opacity", String.valueOf(opacity));
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("fill", "none");
        args.add(arg4);
        args.add(arg1);
        styles.add(arg2);
        styles.add(arg3);

        float rX = x * squareSize + x * blockSize * 2 + blockSize / 2;
        float rY = y * squareSize + y * blockSize * 2 + blockSize / 2;
        sMSvgHelper.rect(Math.round(rX), Math.round(rY), String.valueOf(squareSize),
            String.valueOf(squareSize), styles, args);
        val = hexVal(39 - i, 40 - i);
        opacity = opacity(val);
        fill = fillColor(Math.round(val));
        arg1.clear();
        arg1.put("stroke", fill);
        arg2.clear();
        arg2.put("stroke-width", String.valueOf(blockSize) + "px");
        arg3.clear();
        arg3.put("opacity", String.valueOf(opacity));
        arg4.clear();
        arg4.put("fill", "none");
        args.clear();
        args.add(arg4);
        args.add(arg1);
        styles.clear();
        styles.add(arg2);
        styles.add(arg3);

        float rX2 = x * squareSize + x * blockSize * 2 + blockSize / 2 + blockSize * 2;
        float rY2 = y * squareSize + y * blockSize * 2 + blockSize / 2 + blockSize * 2;
        sMSvgHelper.rect(Math.round(rX2), Math.round(rY2), String.valueOf(blockSize * 3),
            String.valueOf(blockSize * 3), args, styles);
        i++;
      }
    }
  }

  public static void plaid() {
    float height = 0;
    float width = 0;
    // Horizontal Stripes
    int i = 0;
    float times = 0;
    while (times++ <= 18) {
      float space = hexVal(i, i + 1);
      height += space + 5;
      float val = hexVal(i + 1, i + 2);
      float opacity = opacity(val);
      String fill = fillColor(Math.round(val));
      float stripeHeight = val + 5;
      ArrayList<Map<String, String>> args = new ArrayList<>();
      Map<String, String> arg1 = new HashMap<>();
      arg1.put("opacity", String.valueOf(opacity));
      Map<String, String> arg2 = new HashMap<>();
      arg2.put("fill", fill);
      args.add(arg1);
      args.add(arg2);
      sMSvgHelper.rect(0, Math.round(height), "100%", String.valueOf(stripeHeight), null, args);
      height += stripeHeight;
      i += 2;
    }
    // Vertical Stripes
    i = 0;
    times = 0;
    while (times++ <= 18) {
      float space = hexVal(i, i + 1);
      width += space + 5;
      float val = hexVal(i + 1, i + 2);
      float opacity = opacity(val);
      String fill = fillColor(Math.round(val));
      float stripeWidth = val + 5;
      ArrayList<Map<String, String>> args = new ArrayList<>();
      Map<String, String> arg1 = new HashMap<>();
      arg1.put("opacity", String.valueOf(opacity));
      Map<String, String> arg2 = new HashMap<>();
      arg2.put("fill", fill);
      args.add(arg1);
      args.add(arg2);
      sMSvgHelper.rect(Math.round(width), 0, String.valueOf(stripeWidth), "100%", null, args);
      width += stripeWidth;
      i += 2;
    }
    sMSvgHelper.setWidth(Math.round(width));
    sMSvgHelper.setHeight(Math.round(height));
  }

  public static void triangles() {
    float scale = hexVal(0, 1);
    float sideLength = map(scale, 0, 15, 15, 80);
    float triangleHeight = (float) (sideLength / 2 * Math.sqrt(3));
    String triangle = buildTriangleShape(sideLength, triangleHeight);
    sMSvgHelper.setWidth(Math.round(sideLength * 3));
    sMSvgHelper.setHeight(Math.round(triangleHeight * 6));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i+1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("fill", fill);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("fill-opacity", String.valueOf(opacity));
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("stroke", STROKE_COLOR);
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("stroke-opacity", STROKE_OPACITY);
        args.add(arg1);
        args.add(arg2);
        args.add(arg3);
        args.add(arg4);

        int rotation = 0;
        if (y % 2 == 0) {
          rotation = (x % 2 == 0) ? 180 : 0;
        } else {
          rotation = (x % 2 != 0) ? 180 : 0;
        }
        float halfSideLength = sideLength / 2;
        float halfTriangleHeight = triangleHeight / 2;
        float yTriangleHeight = triangleHeight * y;
        float t1 = (float) (x * sideLength * 0.5 - sideLength / 2);
        Map<String, String> transform = new HashMap<>();
        transform.put("transform", "translate("
            + t1
            + ","
            + yTriangleHeight
            + ") rotate("
            + rotation
            + ", "
            + halfSideLength
            + ", "
            + halfTriangleHeight
            + ")");
        args.add(transform);

        sMSvgHelper.polyline(triangle, null, args);
        // Add an extra one at top-right, for tiling.
        if (x == 0) {
          float xT1 = (float) (6 * sideLength * 0.5 - sideLength / 2);
          args.remove(transform);
          transform.clear();
          transform.put("transform", "translate("
              + xT1
              + ","
              + yTriangleHeight
              + ") rotate("
              + rotation
              + ", "
              + halfSideLength
              + ", "
              + halfTriangleHeight
              + ")");
          args.add(transform);

          sMSvgHelper.polyline(triangle, null, args);
          i++;
        }
      }
    }
  }


  public static void trianglesRotated()
  {
    float scale = hexVal(0, 1);
    float sideLength = map(scale, 0 ,15, 15, 80);
    float triangleWidth = (float) (sideLength / 2 * Math.sqrt(3));
    String triangle = buildRotatedTriangleShape(sideLength, triangleWidth);
    sMSvgHelper.setWidth(Math.round(triangleWidth * 6));
    sMSvgHelper.setHeight(Math.round(sideLength * 3));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i+1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String,String>> args = new ArrayList<>();
        Map<String,String> arg1 = new HashMap<>();
        Map<String,String> arg2 = new HashMap<>();
        Map<String,String> arg3 = new HashMap<>();
        Map<String,String> arg4 = new HashMap<>();
        arg1.put("fill",fill);
        arg2.put("fill-opacity", String.valueOf(opacity));
        arg3.put("stroke",STROKE_COLOR);
        arg4.put("stroke-opacity",STROKE_OPACITY);
        args.add(arg1);
        args.add(arg2);
        args.add(arg3);
        args.add(arg4);


        int rotation = 0;
        if (y % 2 == 0)
          rotation = (x % 2 == 0) ? 180 : 0;
        else
          rotation = (x % 2 != 0) ? 180 : 0;
        float halfSideLength = sideLength / 2;
        float halfTriangleWidth = triangleWidth / 2;
        float xTriangleWidth = x * triangleWidth;
        float t1 = (float) (y * sideLength * 0.5 - sideLength / 2);
        Map<String,String> transform = new HashMap<>();
        transform.put("transform","translate("+xTriangleWidth+", "+ t1+") rotate("+rotation+", "+halfTriangleWidth+", "+halfSideLength+")");
        args.add(transform);
        sMSvgHelper.polyline(triangle, null, args);
        // Add an extra one at top-right, for tiling.
        if (y == 0)
        {
          float yT1 = (float) (6 * sideLength * 0.5 - sideLength / 2);
          args.remove(transform);
          transform.clear();
          transform.put("transform", "translate("
              + xTriangleWidth
              + ", "
              + yT1
              + ") rotate("
              + rotation
              + ", "
              + halfTriangleWidth
              + ", "
              + halfSideLength
              + ")");
          args.add(transform);
          sMSvgHelper.polyline(triangle, null, args);
        }
        i++;
      }
    }
  }

  public static void xes() {
    float squareSize = map(hexVal(0, 1), 0, 15, 10, 25);
    float xSize = (float) (squareSize * 3 * 0.943);
    ArrayList<String> xShape = new ArrayList<>();
    xShape.addAll(Arrays.asList(buildPlusShape(squareSize)));
    sMSvgHelper.setWidth(Math.round(xSize * 3));
    sMSvgHelper.setHeight(Math.round(xSize * 3));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        float dy = (float) ((x % 2 == 0) ? (y * xSize - xSize * 0.5)
            : (y * xSize - xSize * 0.5 + xSize / 4));
        ArrayList<Map<String, String>> args = new ArrayList<>();
        ArrayList<Map<String, String>> styles = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        Map<String, String> arg2 = new HashMap<>();
        arg1.put("fill", fill);
        arg2.put("opacity", String.valueOf(opacity));
        args.add(arg1);
        styles.add(arg2);

        float t1 = x * xSize / 2 - xSize / 2;
        float t2 = dy - y * xSize / 2;
        float halfXSize = xSize / 2;
        Map<String, String> transform = new HashMap<>();
        transform.put("transform",
            "translate(" + t1 + "," + t2 + ") rotate(45, " + halfXSize + ", " + halfXSize + ")");
        args.add(transform);
        sMSvgHelper.group(xShape, styles, args);
        // Add an extra column on the right for tiling.
        if (x == 0) {
          float xT1 = 6 * xSize / 2 - xSize / 2;
          float xT2 = dy - y * xSize / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform", "translate("
              + xT1
              + ","
              + xT2
              + ") rotate(45, "
              + halfXSize
              + ", "
              + halfXSize
              + ")");
          args.add(transform);
          sMSvgHelper.group(xShape, styles, args);
        }
        // Add an extra row on the bottom that matches the first row, for tiling.
        if (y == 0) {
          dy = (x % 2 == 0) ? (6 * xSize - xSize / 2) : (6 * xSize - xSize / 2 + xSize / 4);
          float yT1 = x * xSize / 2 - xSize / 2;
          float yT2 = dy - 6 * xSize / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform", "translate("
              + yT1
              + ","
              + yT2
              + ") rotate(45, "
              + halfXSize
              + ", "
              + halfXSize
              + ")");
          args.add(transform);
          sMSvgHelper.group(xShape, styles, args);
        }
        // These can hang off the bottom, so put a row at the top for tiling.
        if (y == 5) {
          float y2T1 = x * xSize / 2 - xSize / 2;
          float y2T2 = dy - 11 * xSize / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform", "translate("
              + y2T1
              + ","
              + y2T2
              + ") rotate(45, "
              + halfXSize
              + ", "
              + halfXSize
              + ")");
          args.add(transform);
          sMSvgHelper.group(xShape, styles, args);
        }
        // Add an extra one at top-right and bottom-right, for tiling.
        if (x == 0 && y == 0) {
          float xyT1 = 6 * xSize / 2 - xSize / 2;
          float xyT2 = dy - 6 * xSize / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform", "translate("
              + xyT1
              + ","
              + xyT2
              + ") rotate(45, "
              + halfXSize
              + ", "
              + halfXSize
              + ")");
          args.add(transform);
          sMSvgHelper.group(xShape, styles, args);
        }
        i++;
      }
    }
  }


  public static void diamonds()
  {
    float diamondWidth = map(hexVal(0, 1), 0, 15, 10, 50);
    float diamondHeight = map(hexVal(1, 2), 0, 15, 10, 50);
    String diamond = buildDiamondShape(diamondWidth, diamondHeight);
    sMSvgHelper.setWidth(Math.round(diamondWidth*6));
    sMSvgHelper.setHeight(Math.round(diamondHeight * 3));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i+1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String,String>> args = new ArrayList<>();
        Map<String,String> arg1 = new HashMap<>();
        Map<String,String> arg2 = new HashMap<>();
        Map<String,String> arg3 = new HashMap<>();
        Map<String,String> arg4 = new HashMap<>();
        arg1.put("fill",fill);
        arg2.put("fill-opacity", String.valueOf(opacity));
        arg3.put("stroke",STROKE_COLOR);
        arg4.put("stroke-opacity",STROKE_OPACITY);
        args.add(arg1);
        args.add(arg2);
        args.add(arg3);
        args.add(arg4);
        float dx = (y % 2 == 0) ? 0 : (diamondWidth / 2);
        float t1 = x * diamondWidth - diamondWidth / 2 + dx;
        float t2 = diamondHeight / 2 * y - diamondHeight / 2;
        Map<String,String> transform = new HashMap<>();
        transform.put("transform","translate("+t1+", "+t2+")");
        args.add(transform);
        sMSvgHelper.polyline(diamond, null,args);
        // Add an extra one at top-right, for tiling.
        if (x == 0)
        {
          float xT1 = 6 * diamondWidth - diamondWidth / 2 + dx;
          float xT2 = diamondHeight / 2 * y - diamondHeight / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform","translate("+xT1+", "+xT2+")");
          args.add(transform);
          sMSvgHelper.polyline(diamond,null,args);
        }
        // Add an extra row at the end that matches the first row, for tiling.
        if (y == 0)
        {
          float yT1 = x * diamondWidth - diamondWidth / 2 + dx;
          float yT2 = diamondHeight / 2 * 6 - diamondHeight / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform","translate("+yT1+", "+yT2+")");
          args.add(transform);
          sMSvgHelper.polyline(diamond,null,args);
        }
        // Add an extra one at bottom-right, for tiling.
        if (x == 0 && y == 0)
        {
          float xyT1 = 6 * diamondWidth - diamondWidth / 2 + dx;
          float xyT2 = diamondHeight / 2 * 6 - diamondHeight / 2;
          args.remove(transform);
          transform.clear();
          transform.put("transform","translate("+xyT1+", "+xyT2+")");
          args.add(transform);
          sMSvgHelper.polyline(diamond, null,args);
        }
        i++;
      }
    }
  }

  public static void squares() {
    float squareSize = map(hexVal(0, 1), 0, 15, 10, 60);
    sMSvgHelper.setWidth(Math.round(squareSize * 6));
    sMSvgHelper.setHeight(Math.round(squareSize * 6));
    int i = 0;
    for (int y = 0; y <= 5; y++) {
      for (int x = 0; x <= 5; x++) {
        float val = hexVal(i, i + 1);
        float opacity = opacity(val);
        String fill = fillColor(Math.round(val));
        ArrayList<Map<String, String>> args = new ArrayList<>();
        Map<String, String> arg1 = new HashMap<>();
        arg1.put("fill", fill);
        Map<String, String> arg2 = new HashMap<>();
        arg2.put("fill-opacity", String.valueOf(opacity));
        Map<String, String> arg3 = new HashMap<>();
        arg3.put("stroke", STROKE_COLOR);
        Map<String, String> arg4 = new HashMap<>();
        arg4.put("stroke-opacity", STROKE_OPACITY);
        args.add(arg1);
        args.add(arg2);
        args.add(arg3);
        args.add(arg4);
        sMSvgHelper.rect(Math.round(x * squareSize), Math.round(y * squareSize),
            String.valueOf(squareSize), String.valueOf(squareSize), null, args);
        i++;
      }
    }
  }

  private static int hexVal(int begin, int end) {
    return Integer.parseInt(mHash.substring(begin, end), 16);
  }

  private String convertToHex(byte[] data) {
    StringBuilder buf = new StringBuilder();
    for (byte b : data) {
      int halfbyte = (b >>> 4) & 0x0F;
      int two_halfs = 0;
      do {
        buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte)
            : (char) ('a' + (halfbyte - 10)));
        halfbyte = b & 0x0F;
      } while (two_halfs++ < 1);
    }
    return buf.toString();
  }

  public String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    MessageDigest md = MessageDigest.getInstance("SHA-1");
    md.update(text.getBytes("iso-8859-1"), 0, text.length());
    byte[] sha1hash = md.digest();
    return convertToHex(sha1hash);
  }
  //Color functions

  private static float[] hexToRgb(String hexColor) {
    float[] RGB = new float[3];
    float r, g, b;
    hexColor = hexColor.replace("#", "");
    if (hexColor.length() == 3) {
      r = Integer.parseInt(hexColor.substring(0, 1) + (hexColor.substring(0, 1)), 16);
      g = Integer.parseInt(hexColor.substring(1, 2) + (hexColor.substring(1, 2)), 16);
      b = Integer.parseInt(hexColor.substring(2) + (hexColor.substring(2)), 16);
    } else {
      r = Integer.parseInt(hexColor.substring(0, 2), 16);
      g = Integer.parseInt(hexColor.substring(2, 4), 16);
      b = Integer.parseInt(hexColor.substring(4, 6), 16);
    }
    RGB[0] = r;
    RGB[1] = g;
    RGB[2] = b;
    return RGB;
  }

  private static float[] hexToHSV(String hexColor) {
    float H = 0, S, V, R, G, B;
    hexColor = hexColor.replace("#", "");
    float[] HSV = new float[3];
    R = Integer.parseInt(hexColor.substring(0, 1) + hexColor.substring(1, 2), 16);
    G = Integer.parseInt(hexColor.substring(2, 3) + hexColor.substring(3, 4), 16);
    B = Integer.parseInt(hexColor.substring(4, 5) + hexColor.substring(5, 6), 16);
    float var_R = (R / 255);
    float var_G = (G / 255);
    float var_B = (B / 255);
    float var_Min = Math.min(var_R, Math.min(var_G, var_B));
    float var_Max = Math.max(var_R, Math.max(var_G, var_B));
    float del_Max = var_Max - var_Min;
    V = (var_Max + var_Min) / 2;
    if (del_Max == 0) {
      H = 0;
      S = 0;
    } else {
      if (V < 0.5) {
        S = del_Max / (var_Max + var_Min);
      } else {
        S = del_Max / (2 - var_Max - var_Min);
      }
      float del_R = (((var_Max - var_R) / 6) + (del_Max / 2)) / del_Max;
      float del_G = (((var_Max - var_G) / 6) + (del_Max / 2)) / del_Max;
      float del_B = (((var_Max - var_B) / 6) + (del_Max / 2)) / del_Max;
      if (var_R == var_Max) {
        H = del_B - del_G;
      } else if (var_G == var_Max) {
        H = (1 / 3) + del_R - del_B;
      } else if (var_B == var_Max) H = (2 / 3) + del_G - del_R;
      if (H < 0) H++;
      if (H > 1) H--;
    }
    HSV[0] = (H * 360);
    HSV[1] = S;
    HSV[2] = V;
    return HSV;
  }

  private float[] rgbToHsv(int color) {
    float[] HSV = new float[3];
    Color.colorToHSV(color, HSV);
    return HSV;
  }

  private static float[] hsvToRgb(float[] HSV) {
    float[] RGB = new float[3];
    float r, g, b, c, x, m, l = HSV[2], h = HSV[0], s = HSV[1];
    h += 360;
    c = (1 - Math.abs(2 * l - 1)) * s;

    x = c * (1 - Math.abs((h % 60)) - 1);
    m = l - (c / 2);
    if (h < 60) {
      r = c;
      g = x;
      b = 0;
    } else if (h < 120) {
      r = x;
      g = c;
      b = 0;
    } else if (h < 180) {
      r = 0;
      g = c;
      b = x;
    } else if (h < 240) {
      r = 0;
      g = x;
      b = c;
    } else if (h < 300) {
      r = x;
      g = 0;
      b = c;
    } else {
      r = c;
      g = 0;
      b = x;
    }
    r = (r + m) * 255;
    g = (g + m) * 255;
    b = (b + m) * 255;
    RGB[0] = r;
    RGB[1] = g;
    RGB[2] = b;
    return RGB;
  }

  private static String fillColor(int val) {
    return (val % 2 == 0) ? FILL_COLOR_LIGHT : FILL_COLOR_DARK;
  }

  private static float opacity(float val) {

    return map(val, 0, 15, OPACITY_MIN, OPACITY_MAX);
  }

  //Java implementation of Processing's map function
  //http://processing.org/reference/map_.html
  //v for value, d for desired
  public static float map(float value, float vMin, float vMax, float dMin, float dMax) {
    float vRange;
    float dRange;
    vRange = vMax - vMin;
    dRange = dMax - dMin;
    return (value - vMin) * dRange / vRange + dMin;
  }

  public String[] buildChevronShape(float width, float height) {
    float e = (float) (height * 0.66);
    float halfWidth = width / 2;
    float heightMinusE = height - e;
    String[] str = new String[2];
    str[0] = "<polyline points = \"0,0,"
        + halfWidth
        + ","
        + heightMinusE
        + ","
        + halfWidth
        + ","
        + height
        + ",0,"
        + e
        + ",0,0\"/>";
    str[1] = "<polyline points = \""
        + halfWidth
        + ","
        + heightMinusE
        + ","
        + width
        + ",0,"
        + width
        + ","
        + e
        + ","
        + halfWidth
        + ","
        + height
        + ","
        + halfWidth
        + ","
        + heightMinusE
        + "\"/>";
    return str;
  }

  public static String buildTriangleShape(float sideLength, float height) {
    float halfWidth = sideLength / 2;
    return halfWidth
        + ", 0, "
        + sideLength
        + ", "
        + height
        + ", 0, "
        + height
        + ", "
        + halfWidth
        + ", 0";
  }

  public static String buildRotatedTriangleShape(float sideLength, float width)
  {
    float halfHeight = sideLength / 2;
    return "0, 0, "+width+", "+halfHeight+", 0, "+sideLength+", 0, 0";
  }
  public static String buildOctogonShape(float squareSize) {
    float s = squareSize;
    float c = (float) (s * 0.33);
    float sMinusC = s - c;
    return c
        + ",0,"
        + sMinusC
        + ",0,"
        + s
        + ","
        + c
        + ","
        + s
        + ","
        + sMinusC
        + ","
        + sMinusC
        + ","
        + s
        + ","
        + c
        + ","
        + s
        + ",0,"
        + sMinusC
        + ",0,"
        + c
        + ","
        + c
        + ",0";
  }


  public static String buildDiamondShape(float width, float height)
  {
    float halfWidth = width / 2;
    float halfHeight = height / 2;
    return halfWidth+", 0, "+width+", "+halfHeight+", "+halfWidth+", "+height+", 0, "+halfHeight;
  }

  public static String[] buildPlusShape(float squareSize) {
    String[] str = new String[2];
    str[0] = "<rect x=\""
        + squareSize
        + "\" y= \"0\" width=\""
        + squareSize
        + "\" height=\""
        + squareSize * 3
        + " \"/>";
    str[1] = "<rect x=\"0\" y= \""
        + squareSize
        + "\" width=\""
        + squareSize * 3
        + "\" height=\""
        + squareSize
        + " \"/>";
    return str;
  }

  protected static String buildHexagonShape(float sideLength) {
    float c = sideLength;
    float a = c / 2;
    float b = (float) (Math.sin(60 * Math.PI / 180) * c);
    float twoB = b * 2;
    float twoC = c * 2;
    float aPlusC = a + c;
    return "0,"
        + b
        + ","
        + a
        + ",0,"
        + aPlusC
        + ",0,"
        + twoC
        + ","
        + b
        + ","
        + aPlusC
        + ","
        + twoB
        + ","
        + a
        + ","
        + twoB
        + ",0,"
        + b;
  }
}
