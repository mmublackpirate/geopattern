package com.yemyatthu.geopattern;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

public class MainActivity extends ActionBarActivity {
  private Bitmap currentBitmap;
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN) @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    GeoPattern geoPattern = new GeoPattern("This is a simple pattern generator");


    ImageView imageView = (ImageView) findViewById(R.id.text_test);
    try {
      SVG svg = SVG.getFromString(geoPattern.getSVG());
      Picture picture = svg.renderToPicture();
      PictureDrawable drawable = new PictureDrawable(picture);
      imageView.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
      imageView.setImageDrawable(drawable);
    } catch (SVGParseException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
  public int dpToPx(int dp) {
    DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    return px;
  }

}
