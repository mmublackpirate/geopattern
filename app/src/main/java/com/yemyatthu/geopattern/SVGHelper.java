package com.yemyatthu.geopattern;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by yemyatthu on 12/12/14.
 */
public class SVGHelper {
  private int width;
  private int height;
  private String mSvgString;

  public SVGHelper() {
    width = 100;
    height = 100;

    mSvgString = "";
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public String getSvgString() {
    return mSvgString;
  }

  public void setSvgString(String svgString) {
    mSvgString = mSvgString+ svgString;
  }

  public String getSvgHeader() {
    return "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\""
        + width
        + "\" height=\""
        + height
        + "\">";
  }

  public String getSvgCloser() {
    return "</svg>";
  }

  @Override public String toString() {
    return getSvgHeader() + getSvgString() + getSvgCloser();
  }

  public void rect(int x, int y, String width, String height, ArrayList<Map<String, String>> style,
      ArrayList<Map<String,String>> args) {
    setSvgString("<rect x=\""
        + x
        + "\" y=\""
        + y
        + "\" width=\""
        + width
        + "\" height=\""
        + height
        + "\" "
        + writeArgs(style, args)
        + " />");

  }


  public void circle(int cx, int cy, int r, ArrayList<Map<String, String>> style,
      ArrayList<Map<String,String>> args) {
    setSvgString("<circle cx=\"" + cx + "\" cy=\"" + cy + "\" r=\"" + r + "\" " + writeArgs(style,
        args) + "/>");
  }

  public void path(String d, ArrayList<Map<String, String>> style, ArrayList<Map<String,String>> args) {
    setSvgString("<path d=\"" + d + "\" " + writeArgs(style, args) + "/>");
  }

  public void polyline(String points, ArrayList<Map<String, String>> style, ArrayList<Map<String,String>> args) {
    setSvgString("<polyline points=\"" + points + "\" " + writeArgs(style, args) + "/>");
  }

  public void group(ArrayList<String> elements, ArrayList<Map<String, String>> style,
      ArrayList<Map<String,String>> args) {
    String header = "<g " + writeArgs(style, args) + ">";
    String body = "";
    for (String element : elements) {
      body = body + element + "\n";
    }
    String closer = "</g>";
    setSvgString(header + body + closer);
  }

  public String writeArgs(ArrayList<Map<String, String>> style, ArrayList<Map<String, String>> args) {
    String string = "";
    if (style != null) {
      String header = "style=\"";
      for (Map<String, String> s : style){

        string = string + s.keySet().iterator().next() + ":" + s.values().iterator().next() + ";";
      }
      string=header+string.substring(0,string.length()-1)+"\"\n";
    }
    if (args != null) {
      for (Map<String, String> s : args)
        string = string + s.keySet().iterator().next() + "=\"" + s.values().iterator().next() + "\" ";
    }

    return string;
  }
}
